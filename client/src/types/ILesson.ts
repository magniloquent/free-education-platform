/* types/ILesson.ts
 * Written by Magniloquent
 */

export interface ILesson {
  id: number;
  title: string;
  skill: string;
  author: string;
  kind: string;
  data: string;
  xp: number;
};

export interface ILessonType {
  id: number;
  name: string;
  alias: string;
  description: string;
};
