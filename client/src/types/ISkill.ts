/* types/ISkill.ts
 * Written by Magniloquent
 */

export interface ISkillCategory {
  id: number;
  title: string;
  alias: string;
  description: string;
  color1: string;
  color2: string;
};

export interface ISkill {
  id: number;
  title: string;
  author: string;
  description: string;
  categories: ISkillCategory[];
  icon: string;
  lessons: number;
};

export interface ISkillPrereq {
  prereqId: number;
  skillId: number;
  parentId: number;
  parentTitle: string;
};
