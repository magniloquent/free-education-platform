/* components/App.tsx - Main Component
 * Written by Magniloquent <magniloquent@protonmail.com>
 */

import * as React from "react";

function App(): JSX.Element {
  return (
    <h1>
        Hello, world!  
    </h1>
  );
}

export default App;
