/* components/Admin.tsx - Admin Panel
 * Written by Magniloquent <magniloquent@protonmail.com>
 */

import * as React from "react";

import {
  BrowserRouter as Router, 
  Switch, 
  Route, 
  Link
} from "react-router-dom";

import AdminDashboard from "./containers/Dashboard";
import AdminManager from "./containers/Manager";
import AdminModeration from "./containers/Moderation";
import AdminSettings from "./containers/Settings";

import "./Admin.css";

/* Current user icon. */
function HeaderIcon(): JSX.Element {
  return (
    <div className="admin-header-user">
      <div className="left">
        <p>Hello, Admin</p>
        <p>
          <a href="https://cdn.dribbble.com/users/721554/screenshots/3783805/cortex-cms-blogging-interface.gif">Logout</a>
        </p>
      </div>
      <div className="right">    
        <img className="admin-icon" src="https://sss.ukzn.ac.za/wp-content/uploads/2017/12/profile-placeholder.png" alt="User Icon." />
      </div>
    </div>
  );   
}

/* Header for admin panel. */
function AdminHeader(): JSX.Element {
  return (
    <header className="admin-header">
      <HeaderIcon />  
    </header>
  );
}

/* Side navigation for admin panel. */
function AdminNav(): JSX.Element {
  return (
    <aside className="admin-aside">
      <header>
        <h1 style={{color: "white"}}>Admin Panel</h1>
      </header>
      <nav>
        <ul>
          <li>
            <i className="fa fa-compass" />
            <Link to="/">Dashboard</Link>
          </li>
          <li>
            <i className="fa fa-wrench" />
            <Link to="/manager">Manager</Link>
          </li>
          <li>
            <i className="fa fa-user" />
            <Link to="/moderation">Moderation</Link>
          </li>
          <li>
            <i className="fa fa-gear" />
            <Link to="/settings">Settings</Link>
          </li>
        </ul>
      </nav>
      <footer>
        <p>Written by Magniloquent</p>
      </footer>
    </aside>
  );
}

/* Admin main */
function AdminMain(): JSX.Element {
  return (
    <main 
      className="admin-main" 
      style={{overflow: "scroll"}}
    >
      <hr id="admin-header-bar" />
      <div className="container">
        <Switch>
          <Route exact path="/" component={AdminDashboard} />
          <Route path="/manager" component={AdminManager} />
          <Route path="/moderation" component={AdminModeration} />
          <Route path="/settings" component={AdminSettings} />
        </Switch>
      </div>
    </main>
  );
}

/* Admin Page. */
function Admin(): JSX.Element {
  return (
    <Router>
      <div className="admin-page">
        <AdminHeader />
        <AdminNav />
        <AdminMain />
      </div>
    </Router>
  );
}

export default Admin;
