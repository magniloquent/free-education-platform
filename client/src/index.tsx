/* index.tsx - Index
 * Written by Magniloquent <magniloquent@protonmail.com>
 */

import * as React from "react";
import * as ReactDOM from "react-dom";

// import App from "./components/App";
import Admin from "./Admin";
import "./index.css";

ReactDOM.render(
  <Admin />,
  document.getElementById("root")
);
