/* components/SkillSearch.tsx - Skill Search Bar
 * Written by Magniloquent
 */

import * as React from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {ISkill} from "../types/ISkill";
import SkillList from "./SkillList";
import {BASE_URL} from "../util/api";

/* Search bar for skills. */
function SkillSearch(): JSX.Element {
  const [title, setTitle] = React.useState<string>("");
  const [skillList, setSkillList] = React.useState<ISkill[]>([]);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    axios.get(`${BASE_URL}/skills/find?title=${title}`)
      .then(({data}) => {
        const {skills, message} = data;
        setSkillList(skills);
        console.log(message);
      })
      .catch(err => {
        console.log(err);
      }); 
  }

  function handleDelete(id: number) {
    axios.delete(`${BASE_URL}/skills/${id}`)
      .then(({data}) => {
        const {message} = data;
        setSkillList(skillList.filter(s => s.id !== id));
        console.log(message);
      })
      .catch(err => {
        console.log(err);
      });
  }

  return (
    <div className="admin-usersearch">
      <form onSubmit={handleSubmit}>
        <input
          type="text" 
          name="title"
          value={title}
          onChange={(e: { target: HTMLInputElement }) => setTitle(e.target.value)}
        />
        <button type="submit">
          <i className="fa fa-search" />
        </button>
      </form>

      {skillList.length > 0 && (
        <SkillList 
          skills={skillList}
          handleDeleteSkill={handleDelete}
        />
      )}
    </div>
  );
}

export default SkillSearch;
