/* components/EditableLessonList.tsx
 * Written by Magniloquent
 */

import * as React from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {ISkill} from "../types/ISkill";
import {ILesson} from "../types/ILesson";
import {BASE_URL} from "../util/api";

interface ILessonListProps {
  skill: ISkill;
}

/* List of lessons under a skill with options
 * to edit or delete each lesson. */
function EditableLessonList(props: ILessonListProps): JSX.Element {
  const {skill} = props;
  const [lessonList, setLessonList] = React.useState<ILesson[]>([]);

  React.useEffect(() => {
    axios.get(`${BASE_URL}/skills/${skill.id}/lessons`)
      .then(({data}) => {
        const {lessons} = data;
        setLessonList(lessons);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  if (!lessonList) {
    return <p>Loading...</p>;
  }

  return (
    <div className="skill-lesson-editor">
      <form onSubmit={(e) => e.preventDefault()}> 
        <div className="lesson-list">
          {lessonList.map(l => {
            return (
              <div className="lesson-listing">
                <span>{l.title}</span>
                <Link to={`/manager/lessons/edit/${l.id}`}>
                  <button type="button">
                        Edit
                  </button>
                </Link>
                <button type="button">Delete</button>
              </div>
            );
          })}
        </div>
      </form>
    </div>
  );
}

export default EditableLessonList;
