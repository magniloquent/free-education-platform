/* components/LessonForm.tsx
 * Written by Magniloquent
 */

import * as React from "react";
import axios from "axios";
import {ILessonType} from "../types/ILesson";
import {BASE_URL} from "../util/api";

/* Lesson Creation Form */
function LessonForm(): JSX.Element {
  const [title, setTitle] = React.useState<string>("");
  const [kind, setKind] = React.useState<string>("text");
  const [awardedXp, setAwardedXp] = React.useState<number>(100);
  const [skillId, setSkillId] = React.useState<number>(1);
  const [kindList, setKindList] = React.useState<ILessonType[]>([]);

  React.useEffect(() => {
    axios.get(`${BASE_URL}/ltypes`)
      .then(({data}) => {
        const {types} = data;
        setKindList(types);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    axios.post(`${BASE_URL}/lessons`, {
      skillId, title, kind, awardedXp
    })
      .then(({data}) => {
        const {message} = data;
        console.log(message);
      })
      .catch(err => {
        console.log(err);
      });
  }

  if (!kindList) {
    return <p>Loading...</p>;
  }

  return (
    <div className="lesson-form">
      <form onSubmit={handleSubmit}>
        <label 
          className="form-label" 
          htmlFor="title"
        >
          <p>Title: </p>
          <input
            className="form-input"
            name="title" 
            type="text"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            required
          />
        </label>
        <label
          className="form-label"
          htmlFor="kind"
        >
          <p>Kind: </p>
          <select
            className="form-select"
            value={kind}
            onChange={(e) => setKind(e.target.value)}
          >
            {kindList.map(k => {
              return ( 
                <option
                  key={k.id}
                  value={k.alias}
                >
                  {k.name}
                </option>
              );
            })}
          </select>
        </label>
        <label
          className="form-label"
          htmlFor="awardedxp"
        >
          <p>Awarded XP: </p>
          <input
            className="form-input"
            name="awardedxp"
            type="number"
            value={awardedXp}
            onChange={(e) => setAwardedXp(Number(e.target.value))}
            required
          />
        </label>
        <label
          className="form-label"
          htmlFor="skillid"
        >
          <p>Skill Id: </p>
          <input
            className="form-input"
            name="skillid"
            type="number"
            value={skillId}
            onChange={(e) => setSkillId(Number(e.target.value))}
            required
          />
        </label>
        <button
          className="form-button"
          type="submit"
        >
            Create Lesson
        </button>
      </form>
    </div>
  );
}

export default LessonForm;
