/* components/SkillList.tsx
 * Written by Magniloquent
 */

import * as React from "react";
import {Link} from "react-router-dom";
import {ISkill} from "../types/ISkill";

interface ISkillListItemProps {
  key: number;
  skill: ISkill;
  handleDeleteSkill(id: number): void;
}

interface ISkillListProps {
  skills: ISkill[];
  handleDeleteSkill(id: number): void;
};

/* Editable Skill Listing for Admins. */
function SkillListItem(props: ISkillListItemProps): JSX.Element {
  const {skill} = props;
  const {color1, color2} = skill.categories[0];

  return (
    <div className="skill-card">
      <div className="skill-card-settings">
        <Link to={`/manager/skills/edit/${skill.id}`}>
          <i className="fa fa-gear" />
        </Link>
      </div>
      <div className="skill-card-header" style={{background: color1}}>
        <div className="skill-card-icon" style={{border: `4px solid ${color2}`}}>
          <img src={skill.icon} alt="Skill Icon" />
        </div>
        <div className="skill-card-title" style={{background: color2}}>
          <p>{skill.title}</p>
        </div>
      </div>
      <div className="skill-card-body">
        <p>{`Created by: ${skill.author}`}</p>
        <ul className="skill-card-category-list">
          {skill.categories.map(c => {
            return (
              <li
                key={c.id}
                className="skill-card-category"
              >
                <span style={{background: c.color1}}>{c.title}</span>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}

/* Skill list display. */
function SkillList(props: ISkillListProps): JSX.Element {
  const {skills, handleDeleteSkill} = props;

  return (
    <div className="skill-list">
      {skills.map((s: ISkill) => {
        return (
          <SkillListItem 
            key={s.id} 
            skill={s} 
            handleDeleteSkill={handleDeleteSkill}
          />
        );
      })}
    </div>
  );
}

export default SkillList;
