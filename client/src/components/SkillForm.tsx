/* components/SkillForm.tsx
 * Written by Magniloquent
 */

import * as React from "react";
import axios from "axios";
import {ISkill, ISkillCategory} from "../types/ISkill";
import {BASE_URL} from "../util/api";

interface ISkillFormProps {
  skill: ISkill | null;
};

/* Create a skill. */
function SkillForm(props: ISkillFormProps): JSX.Element {
  const {skill} = props;
  const [loading, setLoading] = React.useState<boolean>(true);
  const [title, setTitle] = React.useState<string>(skill ? skill.title : "");
  const [description, setDescription] = React.useState<string>(skill ? skill.description : "");
  const [categories, setCategories] = React.useState<ISkillCategory[]>([]);
  const [skillCats, setSkillCats] = React.useState<ISkillCategory[]>(skill ? skill.categories : []);
  const [currentCat, setCurrentCat] = React.useState<string>("math");
  const [selectOpen, setSelectOpen] = React.useState<boolean>(false);
  const [icon, setIcon] = React.useState<string>(skill ? skill.icon : "");

  React.useEffect(() => {
    axios.get(`${BASE_URL}/categories`)
      .then(({data}) => {
        const cats = data.categories;
        setCategories(cats);
        setLoading(false);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  function clear() {
    setTitle("");
    setDescription("");
    setSkillCats([]);
    setCurrentCat("math");
    setSelectOpen(false);
    setIcon("");
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (skill) {
      axios.put(`${BASE_URL}/skills/${skill.id}`, {
        title, description, categories: skillCats, icon
      })
        .then(({data}) => {
          const {message} = data;
          console.log(message);
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      axios.post(`${BASE_URL}/skills/`, {
        title, description, skillCats, icon
      })
        .then(({data}) => {
          const {message} = data;
          clear();
          console.log(message);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  function addCategory(cat: ISkillCategory) {
    if (!skillCats.find(c => c.id === cat.id)) {
      setSkillCats(cats => [...cats, cat]);
    }
    setSelectOpen(!selectOpen);
  }

  function removeCategory(cat: ISkillCategory) {
    setSkillCats(cats => cats.filter(c => c.id !== cat.id));
    setSelectOpen(false);
  }

  if (loading) {
    return <h1>Loading</h1>;
  }

  return (
    <div className="admin-form">
      <form onSubmit={handleSubmit}>
        <label
          className="form-label"
          htmlFor="title"
        >
            Title: 
          <input
            className="form-input"
            name="title" 
            value={title} 
            placeholder="Untitled Skill"
            onChange={e => setTitle(e.target.value)}
          />
        </label>
        <label
          className="form-label"
          htmlFor="category"
        >
                Category:
          <div className="form-tag-list">
            {skillCats.map(c => {
              return (
                <div className="skill-card-category">
                  <span>{c.alias}</span>
                  <button
                    type="button"
                    style={{background: "none", border: "none", color: "white"}}
                    onClick={() => removeCategory(c)}
                  >
                 &#10006;     
                  </button>
                </div>
              );
            })}
          </div>
          {selectOpen ? (
            <div className="form-tags">
              <select
                className="form-select"
                name="category"
                value={currentCat}
                onChange={e => setCurrentCat(e.target.value)}
              >
                {categories.map(c => {
                  return (
                    <option value={c.alias}>
                      {c.title}
                    </option>  
                  );
                })}
              </select>
              <button 
                type="button"
                onClick={() => addCategory(categories.find(el => el.alias === currentCat))}
              >
                <i className="fa fa-check" />
              </button>
              <button 
                type="button"
                onClick={() => setSelectOpen(!selectOpen)}
              >
                 &#10006;     
              </button>
            </div>
          ) : (
            <button 
              type="button"
              onClick={() => setSelectOpen(!selectOpen)}
            >
+
            </button>
          )}
        </label>
        <label
          className="form-label"
          htmlFor="icon"
        >
            Icon:
          <input
            className="form-input"
            name="icon" 
            placeholder="Example: http://localhost/image.png"
            value={icon}
            onChange={e => setIcon(e.target.value)}
          />
        </label>
        <label
          className="form-label"
          htmlFor="description"
        >
            Description: 
          <textarea 
            className="form-textarea"
            name="description"
            placeholder="Skill description"
            value={description} 
            onChange={e => setDescription(e.target.value)}
          />
        </label>
        <button 
          className="form-button" 
          type="submit"
        >
          {skill ? "Update Skill" : "Create Skill"}
        </button>
      </form>
    </div>
  );
}

export default SkillForm;
