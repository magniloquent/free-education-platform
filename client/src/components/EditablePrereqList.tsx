/* component/EditablePrereqList.tsx
 * Written by Magniloquent
 */

import * as React from "react";
import axios from "axios";
import {ISkill, ISkillPrereq} from "../types/ISkill";
import {BASE_URL} from "../util/api";

interface IPrereqListProps {
  skill: ISkill;
}

/* Form for setting skill prerequisites. */
function EditablePrereqList(props: IPrereqListProps): JSX.Element {
  const {skill} = props;
  const [load, setLoad] = React.useState<boolean>(true);
  const [prereqList, setPrereqList] = React.useState<ISkillPrereq[]>([]);
  const [currentPreId, setCurrentPreId] = React.useState<number>(1);
  const [selectOpen, setSelectOpen] = React.useState<boolean>(false);

  function handleAddPrereq(id: number) {
    axios.post(`${BASE_URL}/skills/${skill.id}/prereqs`, 
      {prereqId: id})
      .then(({data}) => {
        const {message} = data;
        console.log(message);
        setSelectOpen(false);
        setLoad(!load);
      })
      .catch(err => {
        console.log(err);
      });
  }

  function handleDeletePrereq(id: number) {
    axios.delete(`${BASE_URL}/prereqs/${id}`)
      .then(({data}) => {
        const {message} = data;
        console.log(message);
        setLoad(!load);
      })
      .catch(err => {
        console.log(err);
      });
  }
  
  React.useEffect(() => {
    async function getData() {
      axios.get(`${BASE_URL}/skills/${skill.id}/prereqs`)
        .then(({data}) => {
          const {prereqs} = data;
          setPrereqList(prereqs);
        })
        .catch(err => {
          console.log(err);
        });
    }
    getData();
  }, [load]);


  if (!prereqList) {
    return (
      <p>Loading...</p>
    );
  }

  return (
    <div className="prereq-editor">
      <label 
        className="form-label"
        htmlFor="prereq-id"
      >
          Prerequisites:
        <div className="prereq-list">
          {prereqList.map(p => {
            return (
              <div className="prereq">
                <span style={{margin: "0px 1rem 0px 0px"}}>
                  {p.parentTitle}
                </span>
                <button
                  type="button" 
                  onClick={() => handleDeletePrereq(p.prereqId)}
                >
                 &#10006;     
                </button>
              </div>
            );
          })}
        </div>
        {selectOpen ? (
          <form onSubmit={(e) => e.preventDefault()}>
            <input 
              className="form-input"
              type="number"
              name="prereq-id"
              value={currentPreId}
              onChange={(e) => setCurrentPreId(Number(e.target.value))}
            />
            <button 
              type="button" 
              onClick={() => handleAddPrereq(currentPreId)}
            >
              <i className="fa fa-check" />
            </button>
            <button 
              type="button" 
              onClick={() => setSelectOpen(false)}
            >
                 &#10006;     
            </button>
          </form>
        ) : (
          <button
            type="button"
            onClick={() => setSelectOpen(true)}
          >
            <i className="fa fa-plus" />  
          </button>
        )}
      </label>
    </div>
  ); 
}

export default EditablePrereqList;
