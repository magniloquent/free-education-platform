/* containers/Settings.tsx - Admin settings
 * Written by Magniloquent
 */

import * as React from "react";

/* Admin site settings views. */
function AdminSettings(): JSX.Element {
  return (
    <section className="admin-settings">
      <h1>Settings</h1>
      <p>Nothing to see here. Move along.</p>
    </section>
  );
}

export default AdminSettings;
