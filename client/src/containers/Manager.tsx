/* containers/Manager.jsx - Skills and Lesson Management
 * Written by Magniloquent
 */

import * as React from "react";
import {Route, Link} from "react-router-dom";
import axios from "axios";
import { InlineMath, BlockMath } from "react-katex";
import {BASE_URL} from "../util/api";
import {ISkill} from "../types/ISkill";
import {ILesson} from "../types/ILesson";
import SkillSearch from "../components/SkillSearch";
import SkillForm from "../components/SkillForm";
import LessonForm from "../components/LessonForm";
import EditablePrereqList from "../components/EditablePrereqList";
import EditableLessonList from "../components/EditableLessonList";
import "./Manager.css";

interface ISkillFormProps {
  skill: ISkill | null;
};

/* View for creating new skills. */
function CreateSkill(): JSX.Element {
  return (
    <>
      <Link to="/manager">
        <button 
          type="button"
        >
        Back
        </button>
      </Link>
      <h1>Create Skill</h1>
      <SkillForm skill={null} />
    </>
  );
}

/* View for editing existing skills. */
function SkillEditor(props: any): JSX.Element {
  const [skillData, setSkillData] = React.useState<ISkill | null>(null);
  const {match: {params: {skillId}}} = props;

  React.useEffect(() => {
    axios.get(`${BASE_URL}/skills/find?id=${skillId}`)
      .then(({data}) => {
        const {skills} = data;
        const skill = skills[0];
        setSkillData(skill);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  if (!skillData) {
    return <p>Loading...</p>;
  }

  return (
    <div className="admin-skill-editor">
      <Link to="/manager">
        <button 
          type="button"
        >
        Back
        </button>
      </Link>
  
      <h1>Edit Skill</h1>
      <SkillForm skill={skillData} />
      <EditablePrereqList skill={skillData} />
      <EditableLessonList skill={skillData} />    
    </div>
  );
}

/* View for creating new lessons. */
function CreateLesson(): JSX.Element {
  return (
    <>
      <Link to="/manager">
        <button 
          type="button"
        >
        Back
        </button>
      </Link>
      <h1>Create Lesson</h1>
      <LessonForm />
    </>
  );
}

function LessonEditForm(props: any): JSX.Element {
  const {lesson, changeLesson} = props;
  const [title, setTitle] = React.useState<string>(lesson.title || "");
  const [content, setContent] = React.useState<string>(lesson.data || "");

  return (
    <div className="">
      <form>
        <label
          className="form-label"
          htmlFor="title"
        >
          <input
            className="form-input"
            name="title"
            type="text"
            value={title}
            placeholder="Lesson Title"
            onChange={(e) => {
              changeLesson({...lesson, title: e.target.value});
              setTitle(e.target.value);
            }}
          />
        </label>
        <label
          className="form-label"
          htmlFor="content"
        >
          <textarea
            className="form-textarea"
            name="content"
            value={content}
            onChange={(e) => {
              changeLesson({...lesson, data: e.target.value});
              setContent(e.target.value);
            }}
          />
        </label>
        <button 
          className="form-button"
          type="button"
        >
            Submit
        </button>
      </form>
    </div>
  );
}

/* View for editing existing lessons. */
function LessonEditor(props: any): JSX.Element {
  const {match: {params: {lessonId}}} = props;
  const [lessonData, setLessonData] = React.useState<ILesson | null>(null);
  const [preview, setPreview] = React.useState<boolean>(false);

  React.useEffect(() => {
    axios.get(`${BASE_URL}/lessons/${lessonId}`)
      .then(({data}) => {
        const {lesson} = data;
        setLessonData(lesson);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <>
      {preview ? (
        <LessonPreview 
          lesson={lessonData} 
          setPreview={setPreview}
        />
      ) : (
        <div className="admin-lesson-editor">
          <h1>Edit Lesson</h1>
          {lessonData && (
            <LessonEditForm 
              lesson={lessonData} 
              changeLesson={setLessonData} 
              setPreview={setPreview}
            />
          )}
          <button 
            type="button" 
            onClick={() => setPreview(true)}
          >
                Preview Lesson
          </button>
          <button type="button">Publish Lesson</button>
          <button type="button">Delete Lesson</button>
        </div>
      )}
    </>
  );
};

function LessonPreview(props: any): JSX.Element {
  const {lesson, setPreview} = props;
  return (
    <div className="lesson-view">
      <h1>{lesson.title}</h1>
      <p>{lesson.description}</p>
      <BlockMath math={lesson.data} />
      <button 
        type="button" 
        onClick={() => setPreview(false)}
      >
        Back
      </button>
    </div>
  );
}

/* Admin content manager view. */
function AdminManager(): JSX.Element {
  return (
    <section className="admin-manager">
      <h1>Manager</h1>
      <p>Manage platform content.</p>
      <Link to="/manager/skills/create">
        <button 
          type="button"
        >
        New Skill
        </button>
      </Link>      
      <Link to="/manager/lessons/create">
        <button 
          type="button"
        >
        New Lesson
        </button>
      </Link>
      <Route exact path="/manager" component={SkillSearch} />
      <Route exact path="/manager/skills/create" component={CreateSkill} />
      <Route path="/manager/skills/edit/:skillId" component={SkillEditor} />
      <Route exact path="/manager/lessons/create" component={CreateLesson} />
      <Route path="/manager/lessons/edit/:lessonId" component={LessonEditor} />
    </section>
  );
}

export default AdminManager;
