/* containers/Moderation.tsx - User Moderation View 
 * Written by Magniloquent
 */

import * as React from "react";
import axios from "axios";
import {BASE_URL} from "../util/api";

interface IUser {
  id: number;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  age: number;
  joinDate: string;
  level: number;
  xp: number;
};

interface IUserTableProps {
  users: IUser[];
  handleDeleteUser(id: number): void;
};

/* User table display. */
function UserTable(props: IUserTableProps): JSX.Element {
  const {users, handleDeleteUser} = props;

  return (
    <table>
      <tr>
        <th>ID</th>
        <th>Username</th>
        <th>Email</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Joined</th>
        <th>Level</th>
      </tr>
      {users.map((u: IUser) => {
        return (
          <tr key={u.id}>
            <td>{u.id}</td>
            <td>{u.username}</td>
            <td>{u.email}</td>
            <td>{u.firstName || "N/A"}</td>
            <td>{u.lastName || "N/A"}</td>
            <td>{u.age || "N/A"}</td>
            <td>{u.joinDate}</td>
            <td>{u.level}</td>
            <td>
              <button type="button" onClick={() => handleDeleteUser(u.id)}>
                    Delete
              </button>
            </td>
          </tr>
        );
      })}
    </table>
  );
}

/* Search bar for users. */
function UserSearch(): JSX.Element {
  const [name, setName] = React.useState("");
  const [userList, setUserList] = React.useState<IUser[]>([]);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();

    axios.get(`${BASE_URL}/users/find?name=${name}`)
      .then(({data}) => {
        const {users, message} = data;
        setUserList(users);
        console.log(message);
      })
      .catch(err => {
        console.log(err);
      }); 
  }

  function handleDelete(id: number) {
    axios.delete(`${BASE_URL}/users/${id}`)
      .then(({data}) => {
        const {message} = data;
        setUserList(userList.filter(u => u.id !== id));
        console.log(message);
      })
      .catch(err => {
        console.log(err);
      });
  }

  return (
    <div className="admin-usersearch">
      <form onSubmit={handleSubmit}>
        <input
          type="text" 
          name="username"
          value={name}
          onChange={(e: { target: HTMLInputElement }) => setName(e.target.value)}
        />
        <button type="submit">
          <i className="fa fa-search" />
        </button>
      </form>

      {userList.length > 0 && (
      <UserTable 
        users={userList} 
        handleDeleteUser={handleDelete}
      />
      )}
    </div>
  );
}

/* Admin moderation view. */
function AdminModeration(): JSX.Element {
  return (
    <section className="admin-moderation">
      <h1>Moderation</h1>
      <p>Manage content creators and consumers.</p>
      <UserSearch />
    </section>
  );
}

export default AdminModeration;
