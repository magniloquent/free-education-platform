/* containers/Dashboard.tsx - Admin Dashboard
 * Written by Magniloquent
 */

import * as React from "react";

/* Admin dashboard and statistic views. */
function AdminDashboard(): JSX.Element {
  return (
    <section className="admin-dashboard">
      <h1>Dashboard</h1>
      <p>Statistics and whatnot.</p>
    </section>
  );
}

export default AdminDashboard;
