/* dag.js - Directed Acyclic Graph Library
 * I didn't write most of this.
 * Will rewrite soon to stop eslint from complaining.
 */

class DAG {
    constructor() {
        this.adjList = {};
    }

    addVertex(vertex) {
        this.adjList[vertex] = [];
    }

    /* vertex1 would be a skill and vertex2 would be a prereq. */
    addEdge(vertex1, vertex2) {
        if (this.adjList[vertex1]
            && this.adjList[vertex1].includes(vertex2)
        ) {
            return false;
        }

        if (vertex1 === vertex2) {
            return false;
        }

        this.adjList[vertex1].push(vertex2);
        return true;
    }

    dfs() {
        const nodes = Object.keys(this.adjList);
        const visited = {};

        for (let i = 0; i < nodes.length; i++) {
            const node = nodes[i];
            this.dfsUtil(node, visited);
        }
    }

    dfsUtil(vertex, visited) {
        if (!visited[vertex]) {
            visited[vertex] = true;

            const neighbors = this.adjList[vertex];

            for (let i = 0; i < neighbors.length; i++) {
                const neighbor = neighbors[i];
                this.dfsUtil(neighbor, visited);
            }
        }
    }

    detectCycle() {
        const graphNodes = Object.keys(this.adjList);
        const visited = {};
        const recStack = {};

        for (let i = 0; i < graphNodes.length; i++) {
            const node = graphNodes[i];

            if (this.detectCycleUtil(node, visited, recStack)) { return true; }
        }

        return false;
    }

    detectCycleUtil(vertex, visited, recStack) {
        if (!visited[vertex]) {
            visited[vertex] = true;
            recStack[vertex] = true;

            const nodeNeighbors = this.adjList[vertex];
            for (let i = 0; i < nodeNeighbors.length; i++) {
                const currentNode = nodeNeighbors[i];

                if (!visited[currentNode]
                    && this.detectCycleUtil(currentNode, visited, recStack)
                ) {
                    return true;
                } if (recStack[currentNode]) {
                    return true;
                }
            }
        }

        recStack[vertex] = false;
        return false;
    }
}

module.exports = DAG;
