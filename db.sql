-- Database for the education platform

DROP DATABASE IF EXISTS edu;
CREATE DATABASE edu;
USE edu;

-- ### USERS ### --
-- User, will probably break into user and user profile for
-- optimization down the line.
CREATE TABLE `user` (
    id          INT AUTO_INCREMENT NOT NULL,
    username    VARCHAR(64) NOT NULL UNIQUE,
    password    TEXT,
    email       VARCHAR(255) NOT NULL UNIQUE,
    first_name  VARCHAR(255) DEFAULT NULL,
    last_name   VARCHAR(255) DEFAULT NULL,
    age         INT DEFAULT NULL,
    bio         TEXT,
    avatar_url  TEXT,
    join_date   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    xp          INT NOT NULL DEFAULT 0,

    PRIMARY KEY (id)
) ENGINE=InnoDB;

-- Table for roles
CREATE TABLE `role` (
    id          INT NOT NULL UNIQUE,
    title       VARCHAR(255) NOT NULL DEFAULT "Untitled Role",
    description TEXT
) ENGINE=InnoDB;

-- M:N relationship used for authorization
CREATE TABLE `user_role` (
    userid      INT NOT NULL,
    roleid      INT NOT NULL,
    grant_date  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY (userid) REFERENCES user(id),
    FOREIGN KEY (roleid) REFERENCES role(id),
    PRIMARY KEY (userid, roleid, grant_date)
) ENGINE=InnoDB;


-- ### Achievements ### --
-- Table for achievements
CREATE TABLE `achievement` (
    id          INT AUTO_INCREMENT NOT NULL,
    title       VARCHAR(255) NOT NULL DEFAULT "Untitled Achievement",
    description TEXT,

    PRIMARY KEY (id)
) ENGINE=InnoDB;

-- Table for achievements that have been acquired by a user.
CREATE TABLE `user_achievement` (
    userid  INT NOT NULL,
    achid   INT NOT NULL,
    achieved TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY (userid) REFERENCES user(id),
    FOREIGN KEY (achid) REFERENCES achievement(id),
    PRIMARY KEY (userid, achid, achieved)
) ENGINE=InnoDB;


-- ### Skills ### --
-- Skill has a one to many relationship with lesson
CREATE TABLE `skill` (
    id          INT AUTO_INCREMENT NOT NULL,
    title       VARCHAR(255) NOT NULL DEFAULT "Untitled Skill",
    author_id   INT NOT NULL,
    icon_url    TEXT,
    description TEXT,
    released    BOOLEAN NOT NULL DEFAULT FALSE,

    PRIMARY KEY (id),
    FOREIGN KEY (author_id) REFERENCES user(id)
) ENGINE=InnoDB;

-- Prerequisite has many-to-many relationship with skill
-- This table is the bridge to implement required skills to a skill
CREATE TABLE `skill_prereq` (
    id          INT NOT NULL AUTO_INCREMENT,
    skill_id    INT NOT NULL,
    prereq_id   INT NOT NULL,
    released    BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (skill_id) REFERENCES skill(id),
    FOREIGN KEY (prereq_id) REFERENCES skill(id),
    PRIMARY KEY (id, skill_id, prereq_id)
) ENGINE=InnoDB;

-- Category for skill
CREATE TABLE `category` (
    id          INT NOT NULL AUTO_INCREMENT,
    title       VARCHAR(255) NOT NULL DEFAULT "Untitled Skill Category",
    alias       VARCHAR(255) NOT NULL,
    description TEXT,
    color1      VARCHAR(8),
    color2      VARCHAR(8),

    PRIMARY KEY(id)
) ENGINE=InnoDB;

-- M:N Relationship table for skill categories.
-- Each skill can have multiple UNIQUE categories
-- Will add more attributes here later.
CREATE TABLE `skill_category` (
    skill_id    INT NOT NULL,
    cat_id      INT NOT NULL,

    FOREIGN KEY (skill_id) REFERENCES skill(id),
    FOREIGN KEY (cat_id) REFERENCES category(id),
    PRIMARY KEY (skill_id, cat_id)
) ENGINE=InnoDB;

-- Lessons to be filed under a skill.
-- Will enforce order later.
CREATE TABLE `lesson` (
    id          INT AUTO_INCREMENT NOT NULL,
    title       VARCHAR(255) NOT NULL DEFAULT "Untitled Lesson",
    skill_id    INT NOT NULL,
    author_id   INT NOT NULL,
    kind        INT NOT NULL DEFAULT 1,
    data        TEXT,
    awarded_xp  INT NOT NULL DEFAULT 100,
    released    BOOLEAN NOT NULL DEFAULT FALSE,

    PRIMARY KEY (id),
    FOREIGN KEY (skill_id) REFERENCES skill(id),
    FOREIGN KEY (author_id) REFERENCES user(id)
) ENGINE=InnoDB;

-- Reference table for lesson types.
CREATE TABLE `lesson_type` (
    id          INT NOT NULL AUTO_INCREMENT,
    name        VARCHAR(100) NOT NULL UNIQUE,
    alias       VARCHAR(100) NOT NULL UNIQUE,
    description TEXT,

    PRIMARY KEY(id)
);

-- Records for completed lessons
-- Completed lesson has a many-to-many relationship with user
CREATE TABLE `attempted_lesson` (
    user_id         INT NOT NULL,
    lesson_id       INT NOT NULL,
    completed       BOOLEAN DEFAULT FALSE,
    attempt_date    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (lesson_id) REFERENCES lesson(id),
    PRIMARY KEY (user_id, lesson_id, attempt_date)
) ENGINE=InnoDB;


-- ### Groups/Factions ### --
-- Faction, might change name to something more accessible.
CREATE TABLE `faction` (
    id              INT AUTO_INCREMENT NOT NULL,
    name            VARCHAR(255) NOT NULL UNIQUE,
    description     TEXT,
    creation_date   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
) ENGINE=InnoDB;

-- M:N relationship table for members of a facton
CREATE TABLE `faction_member` (
    userid      INT NOT NULL,
    factionid   INT NOT NULL,
    join_date   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY (userid) REFERENCES user(id),
    FOREIGN KEY (factionid) REFERENCES faction(id),
    PRIMARY KEY (factionid, userid, join_date)
) ENGINE=InnoDB;

-- Database Routines
-- If I ever get more developers on board, most likely they will be NodeJS programmers
-- which means SQL is alien to them. I define these so they don't have to mess about
-- with my database. Also mitigates the possibility of inconsistency when querying
-- in different files.

DELIMITER $$


-- Get all users
CREATE PROCEDURE GetUsers()
BEGIN
    SELECT 
        username, 
        email
    FROM
        user;
END $$


-- Get user by id
CREATE PROCEDURE GetUserById(
    IN nid INT
)
BEGIN
    SELECT
        *
    FROM
        user
    WHERE
        id = nid;
END $$


-- Get user by email
CREATE PROCEDURE GetUserByEmail(
    IN nemail VARCHAR(255)
)
BEGIN
    SELECT
        email
    FROM
        user
    WHERE
        email
    LIKE
        CONCAT("%", nemail ,"%"); 
END $$


-- Get user by email
CREATE PROCEDURE GetUserByName(
    IN nname VARCHAR(64)
)
BEGIN
    SELECT
        id as id,
        username as username,
        email as email,
        first_name as firstName,
        last_name as lastName,
        age as age,
        join_date as joinDate,
        xp as xp
    FROM
        user
    WHERE
        username
    LIKE
        CONCAT("%", nname ,"%"); 
END $$

-- Create a user
CREATE PROCEDURE CreateUser(
    IN nuser  VARCHAR(100),
    IN nemail VARCHAR(255),
    IN npass  TEXT
)
BEGIN
    DECLARE u_user_id INT DEFAULT 0;
    START TRANSACTION;

    INSERT INTO
        user (
            username,
            email,
            password
        )
    VALUES (
        nuser,
        nemail,
        npass
    );

    SET u_user_id = LAST_INSERT_ID();

    IF u_user_id > 0 THEN
        CALL GetUserById(u_user_id);
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;
END $$


-- Delete a user
CREATE PROCEDURE DeleteUser(
    IN nuid INT
)
BEGIN
    DELETE FROM
        user
    WHERE
        id = nuid;
END $$


-- Get Categories
CREATE PROCEDURE GetCategories()
BEGIN
    SELECT
        *
    FROM
        category;
END $$


-- Get all categories a skill is listed under
CREATE PROCEDURE GetCategoriesBySkill(
    IN nskillid INT
)
BEGIN
    SELECT
        c.*
    FROM
        skill_category sc
    INNER JOIN
        category c
    ON
        sc.cat_id = c.id
    WHERE
        sc.skill_id = nskillid;
END $$


-- Get a category by its id
CREATE PROCEDURE GetCategoryById(
    IN nid INT
)
BEGIN
    SELECT
        *
    FROM
        category
    WHERE
        id = nid;
END $$


-- Get a category by short name
CREATE PROCEDURE GetCategoryByAlias(
    IN nalias VARCHAR(255)
)
BEGIN
    SELECT
        *
    FROM
        category
    WHERE
        alias = nalias;
END $$


-- Create a category
CREATE PROCEDURE CreateCategory(
    IN ntitle VARCHAR(255),
    IN nalias VARCHAR(255),
    IN ndesc TEXT,
    IN ncolor1 VARCHAR(8),
    IN ncolor2 VARCHAR(8)
)
BEGIN
    DECLARE c_cat_id INT DEFAULT 0;
    START TRANSACTION;

    INSERT INTO
        category (
            title,
            alias,
            description,
            color1,
            color2
        )
    VALUES (
        ntitle,
        nalias,
        ndesc,
        ncolor1,
        ncolor2
    );

    SET c_cat_id = LAST_INSERT_ID();

    IF c_cat_id > 0 THEN
        CALL GetCategoryById(c_cat_id);
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;
END $$


-- Add a category to a skill
CREATE PROCEDURE CreateSkillCategory(
    IN nskillid INT,
    IN ncatid INT
)
BEGIN
    INSERT INTO
        skill_category (
            skill_id,
            cat_id
        )
    VALUES (
        nskillid,
        ncatid
    );
END $$


-- Delete category from skill
CREATE PROCEDURE DeleteSkillCategory(
    IN nskillid INT,
    IN ncatid INT
)
BEGIN
    DELETE FROM
        skill_category
    WHERE
        skill_id = nskillid
    AND
        cat_id = ncatid;
END $$


-- Get all skills
CREATE PROCEDURE GetSkills()
BEGIN
    SELECT 
        s.id as id, 
        s.title as title,
        u.username as author,
        s.description as description, 
        s.icon_url as icon, 
        COUNT(l.id) as lessons
    FROM 
        skill s 
    LEFT JOIN 
        lesson l 
    ON 
        s.id = l.skill_id
    INNER JOIN
        user u
    ON
        u.id = s.author_id
    GROUP BY 
        s.id;
END $$


-- Get a skill
CREATE PROCEDURE GetSkillById(
    IN nid INT
)
BEGIN
    SELECT 
        s.id as id, 
        s.title as title,
        u.username as author,
        s.description as description, 
        s.icon_url as icon, 
        COUNT(l.id) as lessons
    FROM 
        skill s 
    LEFT JOIN 
        lesson l 
    ON 
        s.id = l.skill_id
    INNER JOIN
        user u
    ON
        u.id = s.author_id
    WHERE
        s.id = nid
    GROUP BY 
        s.id;
END $$


-- Get a skill by title
CREATE PROCEDURE GetSkillByTitle(
    IN ntitle VARCHAR(255)
)
BEGIN
    SELECT 
        s.id as id, 
        s.title as title, 
        u.username as author,
        s.description as description, 
        s.icon_url as icon, 
        COUNT(l.id) as lessons 
    FROM 
        skill s 
    LEFT JOIN 
        lesson l 
    ON 
        s.id = l.skill_id
    INNER JOIN
        user u
    ON
        u.id = s.author_id
    WHERE
        s.title
    LIKE
        CONCAT("%", ntitle, "%")
    GROUP BY 
        s.id;
END $$


-- Check if a skill exists
CREATE PROCEDURE CheckSkillExists(
    IN ntitle VARCHAR(255)
)
BEGIN
    SELECT
        title
    FROM
        skill
    WHERE
        title = ntitle;
END $$


-- Create a skill
CREATE PROCEDURE CreateSkill(
    IN ntitle VARCHAR(255),
    IN ndesc TEXT,
    IN nicon TEXT,
    IN nuserid INT
)
BEGIN
    DECLARE s_skill_id INT DEFAULT 0;

    INSERT INTO
        skill(
            title,
            author_id,
            description,
            icon_url
        )
    VALUES (
        ntitle,
        nuserid,
        ndesc,
        nicon
    );

    SET s_skill_id = LAST_INSERT_ID();

    IF s_skill_id > 0 THEN
        CALL GetSkillById(s_skill_id);
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;
END $$


-- Update a skill
CREATE PROCEDURE UpdateSkill(
    IN nid INT,
    IN ntitle VARCHAR(255),
    IN ndesc TEXT,
    IN nicon TEXT
)
BEGIN
    UPDATE
        skill
    SET
        title=ntitle,
        description=ndesc,
        icon_url=nicon
    WHERE
        id = nid;
END $$


-- Add a prerequisite relationship to a skill
CREATE PROCEDURE CreateSkillPrereq(
    IN nsid INT,
    IN npid INT
)
BEGIN
   INSERT INTO
        skill_prereq(
            skill_id,
            prereq_id
        )
    VALUES (
        nsid,
        npid
    );
END $$


-- Remove a prerequisite relationship from a skill
CREATE PROCEDURE DeleteSkillPrereq(
    IN nid INT
)
BEGIN
    DELETE FROM
        skill_prereq
    WHERE
        id = nid;
END $$


-- Get all prerequisites
CREATE PROCEDURE GetPrereqs()
BEGIN
    SELECT
        sp.id as prereqId,
        sp.skill_id as skillId,
        sp.prereq_id as parentId,
        p.title as parentTitle
    FROM 
        skill_prereq sp 
    INNER JOIN 
        skill s 
    ON 
        s.id = sp.skill_id 
    INNER JOIN 
        skill p 
    ON 
        p.id = sp.prereq_id;
END $$


-- List all prerequisites for a skill
CREATE PROCEDURE GetSkillPrereqs(
    IN nid INT
)
BEGIN
    SELECT 
        sp.id as prereqId,
        s.id as skillId,
        p.id as parentId,
        p.title as parentTitle
    FROM 
        skill_prereq sp 
    INNER JOIN 
        skill s 
    ON 
        s.id = sp.skill_id 
    INNER JOIN 
        skill p 
    ON 
        p.id = sp.prereq_id
    WHERE
        sp.skill_id = nid;
END $$


-- Delete a skill
CREATE PROCEDURE DeleteSkill(
    IN nid INT
)
BEGIN
    DELETE FROM
        skill_prereq
    WHERE
        skill_id = nid
    OR
        prereq_id = nid;

    DELETE FROM
        lesson
    WHERE
        skill_id = nid;

    DELETE FROM
        skill
    WHERE
        id = nid;
END $$


-- Create a new lesson
CREATE PROCEDURE CreateLesson(
    IN ntitle VARCHAR(255),
    IN nkind INT,
    IN ndata TEXT,
    IN nxp INT,
    IN nauthorid INT,
    IN nskillid INT
)
BEGIN
    INSERT INTO
        lesson(
            title,
            skill_id,
            author_id,
            kind,
            data,
            awarded_xp
        )
    VALUES (
        ntitle,
        nskillid,
        nauthorid,
        nkind,
        ndata,
        nxp
    );
END $$


-- Get all lessons
CREATE PROCEDURE GetLessons()
BEGIN
    SELECT
        *
    FROM
        lesson;
END $$


-- Get a lesson
CREATE PROCEDURE GetLesson(
    IN nid INT
)
BEGIN
    SELECT
        *
    FROM
        lesson
    WHERE
        id = nid;
END $$


-- Get all lessons from a skill
CREATE PROCEDURE GetSkillLessons(
    IN nid INT
)
BEGIN
    SELECT
        *
    FROM
        lesson
    WHERE
        skill_id = nid;
END $$


-- Delete a lesson from a skill
CREATE PROCEDURE DeleteLesson(
    IN nid INT
)
BEGIN
    DELETE FROM
        lesson
    WHERE
        id = nid;
END $$


-- Get all lesson types
CREATE PROCEDURE GetLessonTypes()
BEGIN
    SELECT
        *
    FROM
        lesson_type;
END $$


-- Get lesson type by alias
CREATE PROCEDURE GetLessonTypeByAlias(
    IN nalias VARCHAR(100)
)
BEGIN
    SELECT
        *
    FROM
        lesson_type
    WHERE
        alias = nalias;
END $$


-- Get all lessons with a certain type
CREATE PROCEDURE GetLessonsByType()
BEGIN
    SELECT
        l.title,
        lt.name
    FROM
        lesson_type lt
    INNER JOIN
        lesson l
    ON
        lt.id = l.kind;
END $$


-- Create new lesson type
CREATE PROCEDURE CreateLessonType(
    IN ntitle VARCHAR(100),
    IN nalias VARCHAR(100),
    IN ndesc TEXT
)
BEGIN
    INSERT INTO
        lesson_type(
            name,
            alias,
            description
        )
    VALUES (
        ntitle,
        nalias,
        ndesc
    );
END $$

DELIMITER ;

CALL CreateUser("test1", "test1@test.com", "test1");
CALL CreateUser("test2", "test2@test.com", "test2");

CALL CreateCategory("Mathematics", "math", "The dankest subject of all.", "#5FA9CC", "#4E8DAC");
CALL CreateCategory("Computer Science", "compsci", "The nerdest subject of all.", "#6EBC82", "#5B9D6F");
CALL CreateCategory("Music", "music", "The coolest subject of all.", "#7A78C3", "#6565A5");

CALL CreateLessonType("Text", "text", "Content is textual.");
CALL CreateLessonType("Multiple Choice", "multic", "Learner is given a multiple choice question to answer.");
CALL CreateLessonType("Drag and Drop", "dragdrop", "Learner is presented with blanks to fill in.");
CALL CreateLessonType("XTerm", "xterm", "Learner is presented with a terminal to interact with.");
CALL CreateLessonType("Code Editor", "code", "Learner is presented with a code editor to create a code solution.");

CALL CreateSkill("Test Skill", "Just a test skill.", "https://image.flaticon.com/icons/png/512/149/149060.png", 1);
CALL CreateSkill("Test Skill 2", "Just a test skill. 2", "https://image.flaticon.com/icons/png/512/149/149060.png", 1);
CALL CreateSkill("Test Skill 3", "Just a test skill. 3", "https://image.flaticon.com/icons/png/512/149/149060.png", 1);

CALL CreateSkillPrereq(2, 1);
CALL CreateSkillPrereq(3, 2);

CALL CreateLesson("Lesson #1", 1, "", 100, 1, 1);
CALL CreateLesson("Lesson #2", 1, "", 100, 1, 1);
CALL CreateLesson("Lesson #3", 1, "", 100, 1, 2);
CALL CreateLesson("Lesson #4", 1, "", 100, 1, 2);
CALL CreateLesson("Lesson #5", 1, "", 100, 1, 3);

CALL CreateSkillCategory(1, 1);
CALL CreateSkillCategory(1, 2);
CALL CreateSkillCategory(1, 3);
CALL CreateSkillCategory(2, 1);
CALL CreateSkillCategory(3, 2);
