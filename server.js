/* server.js - Main Server
 * Copyright (C) 2020, Magniloquent <magniloquent@protonmail.com>
 */

const express = require("express");
const bodyParser = require("body-parser");
const mariadb = require("mariadb");
const cors = require("cors");
const bcrypt = require("bcrypt");
const Graph = require("./dag");

const app = express();
const port = 3000;
const saltRounds = 10;

const pool = mariadb.createPool({
    host: "localhost",
    user: "magni",
    password: "password",
    database: "edu",
    connectionLimit: 5,
});

function getLevelFromXp(xp) {
    return Math.floor(Math.sqrt(xp));
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.get("/", (req, res) => {
    res.json({ message: "Welcome to the REST API!" });
});

/** * Users ** */
/* Get all users. */
app.get("/users", async (req, res) => {
    try {
        const users = await pool.query("CALL GetUsers()");
        res.json({
            users: users[0],
        });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get all users. */
app.get("/users/find", async (req, res) => {
    try {
        const { id, name, email } = req.query;
        let users = null;

        if (id && id !== "") {
            users = await pool.query("CALL GetUserById(?)", [id]);
        } else if (name && name !== "") {
            users = await pool.query("CALL GetUserByName(?)", [name]);
        } else if (email && email !== "") {
            users = await pool.query("CALL GetUserByEmail(?)", [email]);
        }

        if (users && users[0].length > 0) {
            res.json({
                message: "Users were found",
                users: users[0].map((u) => ({
                    ...u, level: getLevelFromXp(u.xp),
                })),
            });
        } else {
            res.json({
                message: "User was not found.",
                users: [],
            });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Register a new user. */
app.post("/users", async (req, res) => {
    const {
        username, email, password1, password2,
    } = req.body;

    const errs = [];

    try {
        if (!username) {
            errs.push({ err: "Please provide a username." });
        }

        if (!email) {
            errs.push({ err: "Please provide an email." });
        }

        if (!password1) {
            errs.push({ err: "Please enter a password." });
        }

        if (!password2 || password1 !== password2) {
            errs.push({ err: "Passwords do not match." });
        }

        if (errs.length > 0) {
            res.json({
                message: "There were errors in the data provided.",
                errs,
            });
        } else {
            const query1 = await pool.query("CALL GetUserByEmail(?)", [email]);

            if (query1[0].length > 0) {
                errs.push({ err: "Email has already been registered." });
            }

            const query2 = await pool.query("CALL GetUserByName(?)", [username]);

            if (query2[0].length > 0) {
                errs.push({ err: "Username has already been taken." });
            }

            if (errs.length > 0) {
                res.json({
                    message: "There were errors in the data provided.",
                    errs,
                });
            } else {
                const hash = await bcrypt.hash(password1, saltRounds);

                await pool.query("CALL CreateUser(?,?,?)", [
                    username, email, hash,
                ]);

                res.json({
                    message: "Successfully created user!",
                });
            }
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Delete a user. */
app.delete("/users/:userId", async (req, res) => {
    const id = req.params.userId;

    try {
        const query = await pool.query("CALL DeleteUser(?)", [id]);

        if (query.affectedRows < 1) {
            res.json({ message: "User doesn't exist." });
        } else {
            res.json({ message: "Successfully deleted user!" });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});


/** * Categories ** */
/* Get all categories. */
app.get("/categories", async (req, res) => {
    try {
        const query = await pool.query("CALL GetCategories()");

        res.json({ categories: query[0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});


/** * Skills ** */
/* Get all skills. */
app.get("/skills", async (req, res) => {
    try {
        const skills = await pool.query("CALL GetSkills()");
        res.json({ skills: skills[0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Create a new skill. */
app.post("/skills", async (req, res) => {
    const {
        title, description, icon, categories,
    } = req.body;
    const authorId = 1; // NOTE(Magni): TEMP

    if (!title
        || !description
        || !categories
        || categories.length < 0
    ) {
        res.json({ message: "Please fill out all fields." });
        return;
    }

    try {
        const query1 = await pool.query("CALL CheckSkillExists(?)", [title]);

        if (query1[0].length > 0) {
            res.json({ message: "Skill already exists." });
        } else {
            const query2 = await pool.query("CALL CreateSkill(?,?,?,?)", [
                title, description, icon || null, authorId,
            ]);

            res.json({ message: "Skill successfully created!" });

            await Promise.all(categories.map(async (c) => {
                const query3 = await pool.query("CALL GetCategoryByAlias(?)", [c.title]);

                if (query3[0].length < 1) {
                    return false;
                }

                const skillId = query2[0][0].id;
                const categoryId = query3[0][0].id;
                await pool.query("CALL CreateSkillCategory(?,?)", [skillId, categoryId]);
                return true;
            }));
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get a list of prerequisites for a skill. */
app.get("/prereqs", async (req, res) => {
    try {
        const prereqs = await pool.query("CALL GetPrereqs()");
        res.json({ prereqs: prereqs[0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get a list of prerequisites for a skill. */
app.get("/prereqs/:prereqId", async (req, res) => {
    const id = req.params.prereqId;

    try {
        const prereqs = await pool.query("CALL GetPrereqById(?)", [id]);
        res.json({ prereqs: prereqs[0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Remove a prerequisite. */
app.delete("/prereqs/:prereqId", async (req, res) => {
    const id = req.params.prereqId;

    try {
        const query = await pool.query("CALL DeleteSkillPrereq(?)", [id]);

        if (query.affectedRows < 1) {
            res.json({ message: "Could not find prereq." });
        } else {
            res.json({ message: "Successfully deleted prereq." });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get a skill by its id. */
app.get("/skills/find", async (req, res) => {
    try {
        const { id, title } = req.query;
        let query1 = null;

        if (id && id !== "") {
            query1 = await pool.query("CALL GetSkillById(?)", [id]);
        } else if (title && title !== "") {
            query1 = await pool.query("CALL GetSkillByTitle(?)", [title]);
        }

        if (query1 && query1[0].length > 0) {
            const skills = await Promise.all(query1[0].map(async (s) => {
                try {
                    const query2 = await pool.query("CALL GetCategoriesBySkill(?)", s.id);
                    const categories = query2[0];
                    return { ...s, categories };
                } catch (err) {
                    console.log(err);
                    return null;
                }
            }));

            res.json({
                message: "Skills were found",
                skills,
            });
        } else {
            res.json({
                message: "Skill was not found.",
                skills: [],
            });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Update a skill. */
app.put("/skills/:skillId", async (req, res) => {
    const id = req.params.skillId;
    const {
        title, description, icon, categories,
    } = req.body;

    if (!title
        || !description
        || !icon
        || !categories
        || categories.length < 1
    ) {
        res.json({ message: "Please fill in all fields." });
        return;
    }

    try {
        const query1 = await pool.query("CALL GetSkillById(?)", [id]);

        if (query1[0].length < 1) {
            res.json({ message: "Skill does not exist." });
            return;
        }

        const query2 = await pool.query("CALL GetCategoriesBySkill(?)", [id]);

        await pool.query("CALL UpdateSkill(?,?,?,?)", [
            id, title, description, icon,
        ]);

        if (categories.length !== query2[0].length) {
            await Promise.all(query2[0].map(async (c) => {
                await pool.query("CALL DeleteSkillCategory(?,?)", [id, c.id]);
            }));

            await Promise.all(categories.map(async (c) => {
                await pool.query("CALL CreateSkillCategory(?,?)", [id, c.id]);
            }));
        }

        res.json({ message: "Skill successfully updated." });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get all lessons from a skill. */
app.get("/skills/:skillId/lessons", async (req, res) => {
    const id = req.params.skillId;

    try {
        const lessons = await pool.query("CALL GetSkillLessons(?)", [id]);
        res.json({ lessons: lessons[0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Create a new lesson. */
app.post("/skills/:skillId/lessons", async (req, res) => {
    const id = req.params.skillId;
    const authorId = 1; // Update later.
    const { title, kind, awardedXp } = req.body;
    const data = "";

    try {
        await pool.query("CALL CreateLesson(?,?,?,?,?,?)", [
            title, id, authorId, kind, data, awardedXp,
        ]);

        res.json({ message: "Lesson successfully created!" });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get all lessons from a skill. */
app.get("/skills/:skillId/prereqs", async (req, res) => {
    const id = req.params.skillId;

    try {
        const prereqs = await pool.query("CALL GetSkillPrereqs(?)", [id]);
        res.json({
            prereqs: prereqs[0],
        });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});


/* Create a new lesson. */
app.post("/skills/:skillId/prereqs", async (req, res) => {
    const { skillId } = req.params;
    const { prereqId } = req.body;

    if (!prereqId) {
        res.json({ message: "Please enter a prereq id." });
        return;
    }

    try {
        const dag = new Graph();
        let result = null;
        const query1 = await pool.query("CALL GetSkills()");
        const query2 = await pool.query("CALL GetPrereqs()");
        const skills = query1[0];
        const prereqs = query2[0];
        skills.forEach((el) => dag.addVertex(el.id));
        prereqs.forEach((el) => dag.addEdge(el.skillId, el.parentId));

        if (dag.addEdge(skillId, prereqId) && !dag.detectCycle()) {
            result = await pool.query("CALL CreateSkillPrereq(?,?)", [
                skillId, prereqId,
            ]);
        }

        if (result) {
            res.json({ message: "Prerequisite successfully created!" });
        } else {
            res.json({ message: "Detected cycle in graph." });
        }
    } catch (err) {
        res.sendStatus(500);
        console.log(err);
    }
});

/* Delete a skill. */
app.delete("/skills/:skillId", async (req, res) => {
    const id = req.params.skillId;

    try {
        const query = await pool.query("CALL DeleteSkill(?)", [id]);
        if (query.affectedRows < 1) {
            res.json({
                message: "Could not delete skill.",
            });
        } else {
            res.json({
                message: "Successfully deleted skill.",
            });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/** * Lessons ** */
/* Get all lessons. */
app.get("/lessons", async (req, res) => {
    try {
        const lessons = await pool.query("CALL GetLessons()");
        res.json({
            lessons: lessons[0],
        });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Create a new lesson. */
app.post("/lessons", async (req, res) => {
    const authorId = 1; // Update later.
    const {
        skillId, title, kind, awardedXp,
    } = req.body;
    const data = {};

    if (!skillId
        || skillId < 1
        || !title
        || !kind
        || !awardedXp
    ) {
        res.json({ message: "Please fill in all required fields." });
        return;
    }

    try {
        const query1 = await pool.query("CALL GetLessonTypeByAlias(?)", kind);

        if (query1[0].length < 1) {
            res.json({ message: "Invalid lesson type." });
        } else {
            const lessonType = query1[0][0];
            await pool.query("CALL CreateLesson(?,?,?,?,?,?)", [
                title, lessonType.id, data, awardedXp, authorId, skillId,
            ]);
            res.json({ message: "Lesson successfully created!" });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get Lesson types. */
app.get("/ltypes", async (req, res) => {
    try {
        const types = await pool.query("CALL GetLessonTypes()");
        res.json({ types: types[0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Get a lesson by ID. */
app.get("/lessons/:lessonId", async (req, res) => {
    const id = req.params.lessonId;

    try {
        const lessons = await pool.query("CALL GetLesson(?)", [id]);
        res.json({ lesson: lessons[0][0] });
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

/* Delete a lesson. */
app.delete("/lessons/:lessonId", async (req, res) => {
    const id = req.params.lessonId;

    try {
        const query = await pool.query("CALL DeleteLesson(?)", [id]);

        if (query.affectedRows < 1) {
            res.json({ message: "Lesson does not exist." });
        } else {
            res.json({ message: "Successfully deleted lesson." });
        }
    } catch (err) {
        console.log(err);
        res.sendStatus(500);
    }
});

app.listen(port, () => {
    console.log(`Listening on port: ${port}!`);
});
